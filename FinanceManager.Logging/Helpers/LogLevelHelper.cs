﻿using FinanceManager.Logging.Enumerations;

namespace FinanceManager.Logging.Helpers
{
    internal static class LogLevelHelper
    {
        public static Serilog.Events.LogEventLevel ToSerilogLevel(this LogLevel logLevel)
        {
            switch (logLevel)
            {
                case LogLevel.Error:
                    return Serilog.Events.LogEventLevel.Error;

                case LogLevel.Warning:
                    return Serilog.Events.LogEventLevel.Warning;

                case LogLevel.Debug:
                    return Serilog.Events.LogEventLevel.Debug;

                default:
                    return Serilog.Events.LogEventLevel.Information;
            }
        }
    }
}