﻿using FinanceManager.Logging.Enumerations;
using FinanceManager.Logging.Helpers;
using Serilog;
using System;

namespace FinanceManager.Logging.Implementations
{
    public class Logger : Interfaces.ILogger
    {
        public Logger(LogLevel minimumLevel)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Is(minimumLevel.ToSerilogLevel())
                .WriteTo.File(
                    "FinanceManagerLogs.log",
                    rollingInterval: RollingInterval.Day,
                    retainedFileCountLimit: 5
                )
                .WriteTo.Debug()
                .CreateLogger();
        }

        public void Debug(string message)
        {
            if (Log.IsEnabled(Serilog.Events.LogEventLevel.Debug))
                Log.Debug(message);
        }

        public void Debug(Exception exception, string message)
        {
            if (Log.IsEnabled(Serilog.Events.LogEventLevel.Debug))
                Log.Debug(exception, message);
        }

        public void Error(string message)
        {
            Log.Error(message);
        }

        public void Error(Exception exception, string message)
        {
            Log.Error(exception, message);
        }

        public void Information(string message)
        {
            if (Log.IsEnabled(Serilog.Events.LogEventLevel.Information))
                Log.Information(message);
        }

        public void Warning(Exception exception, string message)
        {
            Log.Warning(exception, message);
        }

        public void Warning(string message)
        {
            Log.Warning(message);
        }
    }
}