﻿namespace FinanceManager.Logging.Enumerations
{
    public enum LogLevel
    {
        Error,
        Warning,
        Debug,
        Information
    }
}