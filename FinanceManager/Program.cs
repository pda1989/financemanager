﻿using FinanceManager.CsvParser.Enumerations;
using FinanceManager.CsvParser.Exceptions;
using FinanceManager.CsvParser.Implementations;
using FinanceManager.Exceptions;
using FinanceManager.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FinanceManager
{
    internal static class Program
    {
        private static string File = "File";
        private static string Type = "Type";
        private static string GroupByMonth = "GroupByMonth";
        private static string LogLevel = "LogLevel";
        private static string ShowDetails = "ShowDetails";

        private static void Main(string[] args)
        {
            // Read parameters
            var parameters = PrepareParameters(args);
            if (parameters == null)
            {
                Console.WriteLine("Wrong parameters: FinanceManager.exe [/D] [/L LogLevel] /F FileName /T");
                return;
            }
            if (!parameters.TryGetValue(File, out var path))
            {
                Console.WriteLine("Missing file path");
                return;
            }
            if (!parameters.TryGetValue(Type, out var type))
            {
                Console.WriteLine("Missing file type");
                return;
            }
            if (!parameters.TryGetValue(LogLevel, out var minimumLogLevel))
                minimumLogLevel = "Information";
            if (!parameters.TryGetValue(ShowDetails, out var showDetails))
                showDetails = false;
            if (!parameters.TryGetValue(GroupByMonth, out var groupByMonth))
                groupByMonth = false;

            // Init dependencies
            var logger = new Logger((string)minimumLogLevel);
            CsvParser.Configuration.InitLogger(logger);
            var csvParser = new CsvParserFactory().CreateParser(CsvProvider.Revolut);
            var categoriesProvider = new CategoriesProvider(logger);
            var transactionDistributor = new TransactionDistributor(logger);

            logger.Information($"The file path: {path}");
            logger.Information($"Minimum log level: {minimumLogLevel}");
            logger.Information($"Show details: {showDetails}");
            logger.Information($"Group by month: {groupByMonth}");

            try
            {
                var categories = categoriesProvider.GetCategories();

                var transactions = csvParser.Parse((string)path);

                var distributionResults = transactionDistributor.Distribute(transactions, categories, true);

                if (distributionResults != null)
                {
                    foreach (var result in distributionResults)
                    {
                        Console.WriteLine($"{result.Key.Name} ({result.Value.Count}) : {result.Value.Sum(t => t.Amount)}");

                        if ((bool)groupByMonth)
                        {
                            var months = result.Value.GroupBy(t => t.Date.ToString("MMMM yyyy"));
                            foreach (var month in months)
                            {
                                Console.WriteLine($"  {month.Key} : {month.Sum(t => t.Amount)}");
                                if ((bool)showDetails)
                                    month
                                        .OrderByDescending(t => t.Amount).ToList()
                                        .ForEach(t => Console.WriteLine($"    {t}"));
                            }
                        }
                        else
                        {
                            if ((bool)showDetails)
                                result.Value
                                    .OrderByDescending(t => t.Amount).ToList()
                                    .ForEach(t => Console.WriteLine($"  {t}"));
                        }
                    }
                }
            }
            catch (ParsingException exception)
            {
                logger.Error(exception, exception.Message);
                Console.WriteLine(exception.Message);
            }
            catch (DistributeException exception)
            {
                logger.Error(exception, exception.Message);
                Console.WriteLine(exception.Message);
            }
            catch (Exception exception)
            {
                logger.Error(exception, "Unexpected exception");
                throw;
            }
        }

        private static Dictionary<string, object> PrepareParameters(string[] args)
        {
            var result = new Dictionary<string, object>();

            int currentIndex = 0;
            while (currentIndex < args.Length)
            {
                switch (args[currentIndex])
                {
                    case "/L":
                        result.Add("LogLevel", args[currentIndex + 1]);
                        currentIndex += 2;
                        break;

                    case "/F":
                        result.Add("File", args[currentIndex + 1]);
                        currentIndex += 2;
                        break;

                    case "/T":
                        result.Add("Type", args[currentIndex + 1]);
                        currentIndex += 2;
                        break;

                    case "/D":
                        result.Add(ShowDetails, true);
                        currentIndex++;
                        break;

                    case "/M":
                        result[GroupByMonth] = true;
                        currentIndex++;
                        break;

                    default:
                        return null;
                }
            }

            return result;
        }
    }
}