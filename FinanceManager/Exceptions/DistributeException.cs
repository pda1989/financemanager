﻿using System;

namespace FinanceManager.Exceptions
{
    internal class DistributeException : Exception
    {
        public DistributeException(string message) : base(message)
        {
        }

        public DistributeException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}