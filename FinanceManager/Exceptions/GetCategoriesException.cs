﻿using System;

namespace FinanceManager.Exceptions
{
    internal class GetCategoriesException : Exception
    {
        public GetCategoriesException(string message) : base(message)
        {
        }

        public GetCategoriesException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}