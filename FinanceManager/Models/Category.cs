﻿using FinanceManager.CsvParser.Interfaces;
using System;
using System.Text.RegularExpressions;

namespace FinanceManager.Models
{
    public class Category
    {
        public Func<ITransaction, bool> BelongsToCategory =>
            (transaction) => Regex.IsMatch(transaction.ToString(), RegexRule);

        public string Name { get; set; }

        public string RegexRule { get; set; }
    }
}