﻿using FinanceManager.Models;
using System.Collections.Generic;

namespace FinanceManager.Interfaces
{
    internal interface ICategoriesProvider
    {
        IList<Category> GetCategories();
    }
}