﻿using FinanceManager.CsvParser.Interfaces;
using FinanceManager.Models;
using System.Collections.Generic;

namespace FinanceManager.Interfaces
{
    public interface ITransactionDistributor
    {
        Dictionary<Category, List<ITransaction>> Distribute(IList<ITransaction> transactions, IList<Category> categories, bool collectUnknownTransactions = false);
    }
}