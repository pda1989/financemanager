﻿using FinanceManager.CsvParser.Interfaces;
using FinanceManager.Exceptions;
using FinanceManager.Interfaces;
using FinanceManager.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace FinanceManager.Implementations
{
    internal class CategoriesProvider : ICategoriesProvider
    {
        private readonly ILogger _logger;

        public CategoriesProvider(ILogger logger)
        {
            _logger = logger;
        }

        public IList<Category> GetCategories()
        {
            _logger.Information("Get categories from the file Categories.json");

            var categories = new List<Category>();

            try
            {
                using var file = new StreamReader("Categories.json");
                string content = file.ReadToEnd();
                categories.AddRange(JsonConvert.DeserializeObject<List<Category>>(content));
            }
            catch (Exception exception)
            {
                throw new GetCategoriesException("Not possible to read categories from the file", exception);
            }

            _logger.Information($"Categories read from file successfully. Number of categories: {categories.Count}");

            return categories;
        }
    }
}