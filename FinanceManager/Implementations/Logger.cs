﻿using FinanceManager.CsvParser.Interfaces;
using System;

namespace FinanceManager.Implementations
{
    internal class Logger : ILogger
    {
        private Logging.Interfaces.ILogger _logger;

        public Logger(string minimumLogLevel)
        {
            Logging.Enumerations.LogLevel minimumLevel = Logging.Enumerations.LogLevel.Information;

            if (Enum.TryParse(typeof(Logging.Enumerations.LogLevel), minimumLogLevel, out var level))
                minimumLevel = (Logging.Enumerations.LogLevel)level;

            _logger = new Logging.Implementations.Logger(minimumLevel);
        }

        public void Debug(string message)
        {
            _logger.Debug(message);
        }

        public void Debug(Exception exception, string message)
        {
            _logger.Debug(exception, message);
        }

        public void Error(string message)
        {
            _logger.Error(message);
        }

        public void Error(Exception exception, string message)
        {
            _logger.Error(exception, message);
        }

        public void Information(string message)
        {
            _logger.Information(message);
        }

        public void Warning(Exception exception, string message)
        {
            _logger.Warning(exception, message);
        }

        public void Warning(string message)
        {
            _logger.Warning(message);
        }
    }
}