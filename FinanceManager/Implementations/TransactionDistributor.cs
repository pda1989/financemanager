﻿using FinanceManager.CsvParser.Interfaces;
using FinanceManager.Exceptions;
using FinanceManager.Interfaces;
using FinanceManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FinanceManager.Implementations
{
    public class TransactionDistributor : ITransactionDistributor
    {
        private readonly ILogger _logger;

        public TransactionDistributor(ILogger logger)
        {
            _logger = logger;
        }

        public Dictionary<Category, List<ITransaction>> Distribute(IList<ITransaction> transactions, IList<Category> categories, bool collectUnknownTransactions = false)
        {
            _logger.Information($"Distribute {transactions.Count} transactions among {categories.Count} categories");

            var result = new Dictionary<Category, List<ITransaction>>();

            try
            {
                foreach (var category in categories)
                {
                    var filteredTransactions = transactions.Where(category.BelongsToCategory);

                    _logger.Debug($"For the \"{category.Name}\" category found {filteredTransactions.Count()} transactions");

                    result.Add(category, filteredTransactions.ToList());
                }

                if (collectUnknownTransactions)
                {
                    var unknownTransactions = transactions.Except(result.Values.SelectMany(t => t));
                    result.Add(new Category { Name = "Other" }, unknownTransactions.ToList());
                }
            }
            catch (Exception exception)
            {
                throw new DistributeException("Not possible to distribute transactions", exception);
            }

            _logger.Information("Distribution is finished successfully");

            return result;
        }
    }
}