﻿using System.Collections.Generic;

namespace FinanceManager.CsvParser.Interfaces
{
    public interface ICsvParser
    {
        IList<ITransaction> Parse(string path);
    }
}