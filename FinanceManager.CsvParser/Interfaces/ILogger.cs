﻿using System;

namespace FinanceManager.CsvParser.Interfaces
{
    public interface ILogger
    {
        void Debug(string message);

        void Debug(Exception exception, string message);

        void Error(string message);

        void Error(Exception exception, string message);

        void Information(string message);

        void Warning(Exception exception, string message);

        void Warning(string message);
    }
}