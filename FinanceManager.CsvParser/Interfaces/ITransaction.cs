﻿using FinanceManager.CsvParser.Enumerations;
using System;

namespace FinanceManager.CsvParser.Interfaces
{
    public interface ITransaction
    {
        string Account { get; }

        double Amount { get; }

        string Currency { get; }

        DateTime Date { get; }

        string Description { get; }

        string RecipientAccount { get; }

        string RecipientName { get; }

        TransactionType Type { get; }
    }
}