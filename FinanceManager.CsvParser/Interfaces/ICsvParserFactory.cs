﻿using FinanceManager.CsvParser.Enumerations;

namespace FinanceManager.CsvParser.Interfaces
{
    public interface ICsvParserFactory
    {
        ICsvParser CreateParser(CsvProvider csvProvider);
    }
}
