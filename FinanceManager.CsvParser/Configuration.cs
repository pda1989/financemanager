﻿using FinanceManager.CsvParser.Interfaces;

namespace FinanceManager.CsvParser
{
    public static class Configuration
    {
        public static ILogger Logger { get; private set; }

        public static void InitLogger(ILogger logger)
        {
            Logger = logger;
        }
    }
}