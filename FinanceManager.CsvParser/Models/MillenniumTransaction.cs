﻿using CsvHelper.Configuration.Attributes;
using FinanceManager.CsvParser.Converters;
using FinanceManager.CsvParser.Enumerations;
using FinanceManager.CsvParser.Interfaces;
using System;

namespace FinanceManager.CsvParser.Models
{
    public class MillenniumTransaction : ITransaction
    {
        [Index(0)]
        public string Account { get; set; }

        public double Amount
        {
            get => ExpenseAmount == 0 ?
                IncomeAmount :
                ExpenseAmount;
        }

        [Index(10)]
        public string Currency { get; set; }

        [Index(1)]
        public DateTime Date { get; set; }

        [Index(6)]
        public string Description { get; set; }

        [Index(7)]
        [TypeConverter(typeof(AmountTypeConverter))]
        public double ExpenseAmount { get; set; }

        [Index(8)]
        [TypeConverter(typeof(AmountTypeConverter))]
        public double IncomeAmount { get; set; }

        [Index(4)]
        public string RecipientAccount { get; set; }

        [Index(5)]
        public string RecipientName { get; set; }

        [Index(3)]
        [TypeConverter(typeof(MillenniumTransactionTypeConverter))]
        public TransactionType Type { get; set; }

        public override string ToString()
        {
            return $"[{Type}] [{Date}] \"{Description}\" : {Amount} {Currency}";
        }
    }
}