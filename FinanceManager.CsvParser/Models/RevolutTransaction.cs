﻿using System;
using CsvHelper.Configuration.Attributes;
using FinanceManager.CsvParser.Converters;
using FinanceManager.CsvParser.Enumerations;
using FinanceManager.CsvParser.Interfaces;

namespace FinanceManager.CsvParser.Models
{
    public class RevolutTransaction : ITransaction
    {
        public string Account => "Revolut";

        public double Amount
        {
            get => ExpenseAmount == 0 ?
                IncomeAmount :
                ExpenseAmount;
        }

        public string Currency => "PLN";

        [Index(0)]
        public DateTime Date { get; set; }

        [Index(1)]
        public string Description { get; set; }

        [Index(2)]
        [TypeConverter(typeof(AmountTypeConverter))]
        public double ExpenseAmount { get; set; }

        [Index(3)]
        [TypeConverter(typeof(AmountTypeConverter))]
        public double IncomeAmount { get; set; }

        [Ignore]
        public string RecipientAccount { get; set; }

        [Ignore]
        public string RecipientName { get; set; }

        [Index(8)]
        [TypeConverter(typeof(RevolutTransactionTypeConverter))]
        public TransactionType Type { get; set; }

        public override string ToString()
        {
            return $"[{Type}] [{Date}] \"{Description}\" : {Amount} {Currency}";
        }
    }
}
