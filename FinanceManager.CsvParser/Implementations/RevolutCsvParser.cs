﻿using System;
using System.Collections.Generic;
using FinanceManager.CsvParser.Interfaces;
using FinanceManager.CsvParser.Models;

namespace FinanceManager.CsvParser.Implementations
{
    public class RevolutCsvParser : CsvParserBase
    {
        public RevolutCsvParser()
        {
            _csvProvider = Enumerations.CsvProvider.Revolut;
        }

        public override IList<ITransaction> Parse(string path)
        {
            return Parse<RevolutTransaction>(path);
        }
    }
}
