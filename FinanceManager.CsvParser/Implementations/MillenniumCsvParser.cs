﻿using FinanceManager.CsvParser.Interfaces;
using FinanceManager.CsvParser.Models;
using System.Collections.Generic;

namespace FinanceManager.CsvParser.Implementations
{
    public class MillenniumCsvParser : CsvParserBase
    {
        public MillenniumCsvParser()
        {
            _csvProvider = Enumerations.CsvProvider.Millennium;
        }

        public override IList<ITransaction> Parse(string path)
        {
            return Parse<MillenniumTransaction>(path);
        }
    }
}