﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using CsvHelper;
using FinanceManager.CsvParser.Enumerations;
using FinanceManager.CsvParser.Exceptions;
using FinanceManager.CsvParser.Interfaces;

namespace FinanceManager.CsvParser.Implementations
{
    public abstract class CsvParserBase : ICsvParser
    {
        protected readonly ILogger _logger = Configuration.Logger;

        protected CsvProvider _csvProvider = CsvProvider.Undefined;

        public abstract IList<ITransaction> Parse(string path);

        protected IList<ITransaction> Parse<T>(string path)
            where T : ITransaction
        {
            _logger.Information($"Parse {_csvProvider} CSV file: {path}");

            var transactions = new List<ITransaction>();

            try
            {
                using (var reader = new StreamReader(path))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    csv.Configuration.BadDataFound = null;
                    csv.Configuration.HeaderValidated = null;
                    csv.Configuration.Delimiter = ";";
                    transactions.AddRange((IEnumerable<ITransaction>)csv.GetRecords<T>());
                }
            }
            catch (Exception exception)
            {
                throw new ParsingException("Not possible to parse the file", exception);
            }

            _logger.Information($"Parsing is finished successfully. Number of transactions: {transactions.Count}");

            return transactions;
        }
    }
}
