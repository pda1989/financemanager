﻿using System;
using FinanceManager.CsvParser.Enumerations;
using FinanceManager.CsvParser.Interfaces;

namespace FinanceManager.CsvParser.Implementations
{
    public class CsvParserFactory : ICsvParserFactory
    {
        public CsvParserFactory()
        {
        }

        public ICsvParser CreateParser(CsvProvider csvProvider)
        {
            switch (csvProvider)
            {
                case CsvProvider.Millennium:
                    return new MillenniumCsvParser();
                case CsvProvider.Revolut:
                    return new RevolutCsvParser();
                default:
                    throw new ArgumentException("Provider type is undefined");
            }
        }
    }
}