﻿namespace FinanceManager.CsvParser.Enumerations
{
    public enum CsvProvider
    {
        Undefined,
        Millennium,
        Revolut
    }
}