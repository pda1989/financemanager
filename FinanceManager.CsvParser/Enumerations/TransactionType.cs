﻿namespace FinanceManager.CsvParser.Enumerations
{
    public enum TransactionType
    {
        Undefined,
        Payment,
        Withdraw,
        Transfer,
        Income
    }
}