﻿using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using System;
using System.Text.RegularExpressions;

namespace FinanceManager.CsvParser.Converters
{
    internal class AmountTypeConverter : ITypeConverter
    {
        public object ConvertFromString(string text, IReaderRow row, MemberMapData memberMapData)
        {
            var result = Regex.Replace(text, "[\\,\\.]", ".");

            if (double.TryParse(result.Trim(), out var value))
                return Math.Abs(value);

            if (string.IsNullOrWhiteSpace(text))
                return (double)0;

            throw new ArgumentException($"Cannot convert \"{text}\" to Double");
        }

        public string ConvertToString(object value, IWriterRow row, MemberMapData memberMapData)
        {
            throw new NotImplementedException();
        }
    }
}