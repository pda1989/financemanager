﻿using System;
using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using FinanceManager.CsvParser.Enumerations;

namespace FinanceManager.CsvParser.Converters
{
    public class RevolutTransactionTypeConverter : ITypeConverter
    {
        public object ConvertFromString(string text, IReaderRow row, MemberMapData memberMapData)
        {
            var logger = Configuration.Logger;

            TransactionType transactionType;
            switch (text)
            {
                case "Transport":
                case "Shopping":
                case "Travel":
                case "Services":
                case "Groceries":
                case "Restaurants":
                    transactionType = TransactionType.Payment;
                    break;

                case "General":
                    transactionType = TransactionType.Income;
                    break;

                default:
                    transactionType = TransactionType.Undefined;
                    break;
            }

            if (transactionType == TransactionType.Undefined)
                logger.Warning($"The \"{text}\" type is undefined type of transaction");
            else
                logger.Debug($"The \"{text}\" type is converted to \"{transactionType}\" transaction type");

            return transactionType;
        }

        public string ConvertToString(object value, IWriterRow row, MemberMapData memberMapData)
        {
            throw new NotImplementedException();
        }
    }
}
