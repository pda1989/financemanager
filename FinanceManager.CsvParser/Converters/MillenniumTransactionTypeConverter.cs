﻿using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using FinanceManager.CsvParser.Enumerations;
using System;

namespace FinanceManager.CsvParser.Converters
{
    internal class MillenniumTransactionTypeConverter : ITypeConverter
    {
        public object ConvertFromString(string text, IReaderRow row, MemberMapData memberMapData)
        {
            var logger = Configuration.Logger;

            TransactionType transactionType;
            switch (text)
            {
                case "TRANSAKCJA KARTĄ PŁATNICZĄ":
                case "PŁATNOŚĆ BLIK W INTERNECIE":
                case "ZAKUP BILETU MIEJSKIEGO":
                case "OPŁATA":
                case "PROWIZJA":
                    transactionType = TransactionType.Payment;
                    break;

                case "WYPŁATA KARTĄ Z BANKOMATU":
                case "WYPŁATA BLIK Z BANKOMATU":
                    transactionType = TransactionType.Withdraw;
                    break;

                case "PRZELEW NATYCHMIASTOWY WYCHODZĄCY":
                case "PRZELEW DO INNEGO BANKU":
                case "OPERACJE SO NA KREDYTACH":
                    transactionType = TransactionType.Transfer;
                    break;

                case "PRZELEW PRZYCHODZĄCY":
                case "OPERACJE NA KREDYTACH":
                    transactionType = TransactionType.Income;
                    break;

                default:
                    transactionType = TransactionType.Undefined;
                    break;
            }

            if (transactionType == TransactionType.Undefined)
                logger.Warning($"The \"{text}\" type is undefined type of transaction");
            else
                logger.Debug($"The \"{text}\" type is converted to \"{transactionType}\" transaction type");

            return transactionType;
        }

        public string ConvertToString(object value, IWriterRow row, MemberMapData memberMapData)
        {
            throw new NotImplementedException();
        }
    }
}